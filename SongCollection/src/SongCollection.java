/**
 * This programme acts as a database for a number of music albums. It shows a menu up to the user and allows them to
 * create albums and add songs, delete albums and songs, and also has functionality to display data about these
 * songs and albums once they have been created
 *
 * @Author: Lachlan Court
 * @Modified 04/05/2020
 * @Version 1.0
 * @StudentNumber: c3308061
 */

//Allows GUI functionality for input/output
import javax.swing.*;

public class SongCollection
{
    private Album album1, album2, album3;

    //The menu item the user selects will be stored here
    private String menuUserInput;
    //The menu input will be converted to an Integer so that it can be checked using the validMenuEntry method
    private int action = 0;

    public void run()
    {
        //Loop will run until the user types a 9, which is exit. Any other entry will mean the loop will
        while (action != 9)
        {
            //Reset menuUserInput variable after the last action
            menuUserInput = "0";
            do
            {
                //Show the user the menu, and ask for the next action to be performed
                menuUserInput = input("1: Create album\n2: Add song\n3: Show song details from an album\n4: Show a list of albums with their songs\n5: Show a list of all songs under a specific duration\n6: Show a list of songs of a specific genre\n7: Delete an album\n8: Delete a song from an album\n9: Exit\nWhat do you wish to do?");
            }
            /*
            Ensure that the user has entered a valid number between 1 and 9. If they have not, the validMenuEntry
            method will loop and ask the user again until they do
             */
            while (!validMenuEntry(menuUserInput, 1, 9));

            //Convert the user input to an integer
            action = Integer.parseInt(menuUserInput);

            //A switch case will determine which menu item the user selected, and call the appropriate method
            switch (action)
            {
                case 1:
                {
                    createAlbum_Menu();
                    break;
                }
                case 2:
                {
                    addSong_Menu();
                    break;
                }
                case 3:
                {
                    showSongDetails_Menu();
                    break;
                }
                case 4:
                {
                    showAlbums_Menu();
                    break;
                }
                case 5:
                {
                    showSongsUnderDuration_Menu();
                    break;
                }
                case 6:
                {
                    showSongsOfSpecificGenre_Menu();
                    break;
                }
                case 7:
                {
                    deleteAlbum_Menu();
                    break;
                }
                case 8:
                {
                    deleteSong_Menu();
                    break;
                }
            }
        }
        //After the while loop has ended, produce a final goodbye message before the program terminates
        output("See you soon!");
    }

    public static void main(String[] args)
    {
        SongCollection sg = new SongCollection();
        sg.run();
    }

    //--------------------- Menu functions ---------------------
    private void createAlbum_Menu()
    {
        /*
        This method allows the user to create an album in the SongCollection database
         */

        //Declare the variable to store the name of the album that the user enters
        String albumName;

        /*
        Check each album and see if any of them are null. If the album is null it has not been created, which means the
        new album can be stored there. If there are no albums that are null, all 3 have been created and the user must
        delete an album first.
         */
        if ((album1 == null) || (album2 == null) || (album3 == null))
        {
            //If there is a free album, ask the user for it's name
            albumName = input("What is the name of the album?");
        }
        else
        {
            //If there is no free album, notify the user and return to the main menu
            output("There are no free albums available at the moment. Please delete an album before adding another");
            return;
        }

        /*
        Next, ensure that the album is not a duplicate. This is done with a short circuting statement where one by one
        each album is first checked that it exists, and then if and only if the album exists, requesting the name of
        that album and comparing it to the name that the user enters. If any of the three albums have an identical name
        the album already exists.
         */
        if (((album1 != null) && (album1.getName().toLowerCase().compareTo(albumName.toLowerCase()) == 0)) || ((album2 != null) && (album2.getName().toLowerCase().compareTo(albumName.toLowerCase()) == 0)) || ((album3 != null) && (album3.getName().toLowerCase().compareTo(albumName.toLowerCase()) == 0)))
        {
            //If an album already exists with the same name, notify the user and return to the main menu
            output("That album already exists!");
            return;
        }

        /*
        There is now an available album and the name entered is not the name of an existing album. Next, find the first
        available album and create a new album with the specified name. Following a convention to always add to the top
        first - that is, if album1 is free add to that, if not, try album2, if not, try album3. The deleteAlbum() method
        ensures that the available albums will always be at the end
         */
        if (album1 == null)
        {
            //If album1 is available, create a new album with the specified name and store there
            album1 = new Album(albumName);
        }
        else if (album2 == null)
        {
            //If album2 is available, create a new album with the specified name and store there
            album2 = new Album(albumName);
        }
        else
        {
            /*
            The availablility of the albums has already been checked. If it's not album1 and it's not album2, album3
            must be free. Therefore it's safe to use an else here. Create a new album with the specified name and store
            ini album3
             */
            album3 = new Album(albumName);
        }

        //And finally show a success message
        output("Album \"" + albumName + "\" created successfully.");
    }

    private void addSong_Menu()
    {
        /*
        This method asks the user to pick an album and then allows the user to add a song to that album. The song has
        user specified name, artist, genre and duration.
         */

        //The following variables will store information about the song as the user enters it
        String songName;
        String songArtist;
        String songGenre = "";
        String songDurationInput;
        //The duration is stored as an integer number of seconds and will be converted and stored in this variable
        int songDuration;
        //The number of the album that the user wants to add add a song to
        int addSongAlbumSelection = 0;
        /*
        After the desired album has been selected, it will be stored in a temporary variable to be edited, then copied
        back at the end
         */
        Album tempAlbum;

        /*
        First call the getAlbumSelection method. This puts together a list of albums and asks the user to select one.
        It returns an integer number 1 being album1, 2 album2, 3 album3 or -1 return to main menu. If no albums have
         been created, it will also notify the user and then return -1 to return to main menu.
         */
        addSongAlbumSelection = getAlbumSelection("Select which album to add a song to");
        if (addSongAlbumSelection == -1)
        {
            //If the user has selected return to main menu the the variable addSongAlbumSelection will be == -1
            return;
        }

        //An album has been selected. Copy the selected album into a temporary working album using a switch case
        switch (addSongAlbumSelection)
        {
            case 1:
            {
                tempAlbum = album1;
                break;
            }
            case 2:
            {
                tempAlbum = album2;
                break;
            }
            default:
                tempAlbum = album3;
        }

        /*
        With the tempAlbum sorted as a place to store the data, we now need to make sure there is a track available on
        the album to store the song. We can do this with the Album Class's isFull method
         */
        if (tempAlbum.isFull())
        {
            // If the album is full, notify the user and then return to the main menu
            output("There are no free tracks on that album at the moment. Please delete a song before adding another");
            return;
        }

        //Having confirmed that there is space on the selected album, start gathering info about the song
        songName = input("What is the name of the song?");
        songArtist = input("What is the artist for the song?");

        /*
        Ask the user to enter a genre. Instead of just typing the genre, the user will be presented with a menu in the
        getGenreSelection method. This method is used in a number of other areas of the program and as such has an
        argument for the prompting message as it will be different depending on which area of the program it is called
        from
         */
        songGenre = getGenreSelection("What is the genre of the song?");

        //Ensure the user enters a valid duration that is not 0 seconds or less
        do
        {
            songDurationInput = input("Enter the duration of the song in the format mm:ss");
        }
        /*
        convertDurationToSecs will return a -1 for any invalid input whether that is not an integer, or a
        misformatted mm:ss entry. A valid entry will return the duration in seconds as an integer number. The duration
        can also not be 0. As such check that the duration is not <= 0
         */
        while (convertDurationToSecs(songDurationInput) <= 0);

        //With a valid duration entered, convert it to seconds to be stored in the Song object
        songDuration = convertDurationToSecs(songDurationInput);

        //Now to check if this is a valid song. First create a temporary song object to run these checks
        Song tempSong = new Song(songName, songArtist, songGenre, songDuration);

        //Next check if the song already exists in the album using the Album Class's hasDuplicate method
        if (tempAlbum.hasDuplicate(tempSong))
        {
            //If the song already exists, notify the user and return to main menu
            output("That song already exists!");
            return;
        }

        //If there is no duplicate, ensure the duration does not exist the maximum playtime for the album
        if (tempAlbum.getTotalTime() + songDuration > tempAlbum.getMaxTime())
        {
            /*
            If the song is going to push the total play time to higher than the maximum playtime, notify the user and
            return to main menu
             */
            output("That song goes for too long! It will not fit on the album.");
            return;
        }

        //If the program has reached this point of the method, the song is valid and can be added to the album
        tempAlbum.addSong(tempSong);

        //Next, copy the tempAlbum back to the correct album that it was copied from before
        switch (addSongAlbumSelection)
        {
            case 1:
            {
                album1 = tempAlbum;
                break;
            }
            case 2:
            {
                album2 = tempAlbum;
                break;
            }
            default:
                album3 = tempAlbum;
        }

        //And finally show a success message
        output("Song \"" + tempSong.getName() + "\" added to album \"" + tempAlbum.getName() + "\" successfully");
    }

    private void showSongDetails_Menu()
    {
        /*
        This method asks the user to pick an album and then shows the songs and details of those songs that are in
        the album that was selected
         */

        //The number of the album that the user wants to add add a song to
        int addSongAlbumSelection = 0;

        /*
        First call the getAlbumSelection method. This puts together a list of albums and asks the user to select one.
        It returns an integer number 1 being album1, 2 album2, 3 album3 or -1 return to main menu
         */
        addSongAlbumSelection = getAlbumSelection("Select which album to show details for");

        if (addSongAlbumSelection == -1)
        {
            //If the user has selected return to main menu the the variable addSongAlbumSelection will be == -1
            return;
        }

        /*
        An album has been selected. Using a switch case, determine which album the user selected and show the song
        details for that album. The showSongs method is used in a number of areas of the program and as such requires a
        boolean argument to control the kind of output received. By passing in the value "true", showSongs will return
        all details of the songs. A value of "false" would only return the song names
         */
        switch (addSongAlbumSelection)
        {
            case 1:
            {
                output(album1.showSongs(true));
                break;
            }
            case 2:
            {
                output(album2.showSongs(true));
                break;
            }
            default:
                output(album3.showSongs(true));
        }
    }

    public void showAlbums_Menu()
    {
        /*
        This method shows all albums that have been created, as well as all songs on those albums
         */

        //This will hold all the details as they are added, and will be printed out in one go at the end
        String albumDetails = "The albums you have in this collection are:\n\n";
        /*
        This variable will keep track of how many albums exist. If it is still 0 at the end of this method, that means
        there are no songs on the album.
         */
        int numberOfAlbums = 0;

        /*
        Check each album individually. If it exists, add the details to albumDetails. Add some blank space at the end
        of each line of details to indent a little bit for neat formatting
         */
        if (album1 != null)
        {
            // If the album exists, the first add the name of the album, and then the songs will be listed afterward
            albumDetails += album1.getName() + "\n    ";
            /*
            The showSongs method is used in a number of areas of the program and as such requires a boolean argument to
            control the kind of output received. By passing in the value "false", showSongs will return only the song
            names. A value of "true" would return all details of the songs.
            */
            albumDetails += album1.showSongs(false) + "\n";
            // Increment the numberOfAlbums variable to keep an accurate count of how many albums currently exist
            numberOfAlbums++;
        }
        // Repeat the above if statement checking album 2
        if (album2 != null)
        {
            albumDetails += album2.getName() + "\n    ";
            albumDetails += album2.showSongs(false) + "\n";
            numberOfAlbums++;
        }
        // Repeat the above if statement checking album 3
        if (album3 != null)
        {
            albumDetails += album3.getName() + "\n    ";//Indent the next line
            albumDetails += album3.showSongs(false) + "\n";//By passing in the value false we ensure we get only the song names. A value of true would return all the details
            numberOfAlbums++;
        }
        /*
         If the method reaches this point and numberOfAlbums is still 0, no albums exist. In this case, replace the
         albumDetails message with an error message
         */
        if (numberOfAlbums == 0)
        {
            albumDetails = "You haven't created any albums! Create some albums first before requesting details.";
        }

        //Finally output the contents of the albumDetails variable to the user
        output(albumDetails);
    }

    public void showSongsUnderDuration_Menu()
    {
        /*
        This method asks the user to specify a duration and then lists all songs in the SongCollection database that
        have a duration under that specific length
         */

        //This will hold the initial input from the user until a positive integer number has been entered.
        String specifiedDurationInput;
        /*
        The user specified duration will be converted to an integer number of seconds. Every song under this duration
        will be added to the output
         */
        int specifiedDuration;
        //This will keep track of all songs under the specified duration and will be displayed at the end of the method
        String songsUnderDuration = "";

        //First check that the user has actually created some albums by comparing each to null
        if (!((album1 != null) || (album2 != null) || (album3 != null)))
        {
            //If all albums are null, notify the user and return to the main menu
            output("You haven't created any albums yet! Create some albums first");
            return;
        }

        //Next, ask the user for the duration that they want to compare to all of the songs in the collection
        do
        {
            specifiedDurationInput = input("Please enter a positive number that is the maximum song duration in the format mm:ss");
        }
        /*
         * Because the algorithm checks songs that are /under/ the specific duration, the user entering a duration of 0
         * is pointless as there can be no song with duration under 0. Therefore the duration that the user enters must
         * not be 0 seconds. The convertDurationToSecs method will return a -1 for any invalid input whether that is not
         * an integer, or a misformatted mm:ss entry. Therefore an invalid input in this case is either a -1, or a 0,
         * in which case simply check that the duration the user specifies is not less than 1.
         */
        while (convertDurationToSecs(specifiedDurationInput) < 1);


        //Now that the user has entered a valid duration, parse it as an integer into specifiedDuration
        specifiedDuration = convertDurationToSecs(specifiedDurationInput);

        /*
        Check each album, and if it exists, call the Album Class's getSongsUnderDuration method to receieve a String of
        all songs under that duration. Add these to the songsUnderDuration String
        It is already known that album1 exists, because earlier in the method the program checked whether there were no
        albums. When an album is created it is always added to the end, and when it is deleted the remaining albums are
        shuffled to the left so it is safe to assume that if any albums exist, album1 definitely exists.
         */
        songsUnderDuration += album1.getSongsUnderDuration(specifiedDuration);
        if (album2 != null)
        {
            songsUnderDuration += album2.getSongsUnderDuration(specifiedDuration);
        }
        if (album3 != null)
        {
            songsUnderDuration += album3.getSongsUnderDuration(specifiedDuration);
        }

        //Now all we need to do is show the result to the user
        if (songsUnderDuration.length() != 0)
        {
            /*
            If songsUnderDuration is not empty, at least one song exists that has a duration under one specified by the
            user. Show this output to the user
             */
            output("The songs in this collection that have a duration under " + specifiedDurationInput + " are as follows:\n\n" + songsUnderDuration);
        }
        else
        {
            //If songsUnderDuration is empty, there are no songs that are under the specified duration
            output("There are no songs in this collection under the specified duration.");
        }
    }

    public void showSongsOfSpecificGenre_Menu()
    {
        /*
        This method asks the user to enter a genre and then displays all songs that are classified as that genre
         */

        //Declare the variable to hold the genre the user specifies
        String specifiedGenre;
        //A running string of all songs of the specified genre which will be displayed at the end of the method
        String songsOfGenre = "";

        //First check that the user has actually created some albums. If not, notify and return to the main menu
        if (!((album1 != null) || (album2 != null) || (album3 != null)))
        {
            output("You haven't created any albums yet! Create some albums first");
            return;
        }

        //Next, ask the user  to enter a genre that they want to compare to all of the songs in the collection
        specifiedGenre = getGenreSelection("Please enter a genre");

        /*Check each album, and if it exists add all songs are of the specific genre using the getSongsOfGenre method.
        It is already known that album1 exists, because earlier in the method the program checked whether there were no
        albums. When an album is created it is always added to the end, and when it is deleted the remaining albums are
        shuffled to the left so it is safe to assume that if any albums exist, album1 definitely exists.
         */
        songsOfGenre += album1.getSongsOfGenre(specifiedGenre);
        if (album2 != null)
        {
            songsOfGenre += album2.getSongsOfGenre(specifiedGenre);
        }
        if (album3 != null)
        {
            songsOfGenre += album3.getSongsOfGenre(specifiedGenre);
        }

        //Now all we need to do is show the result to the user
        if (songsOfGenre.length() != 0)
        {
            /*
            If songsOfGenre is not empty, at least one song exists that has the genre specified by the user. Show this
            output to the user
             */
            output("The songs in this collection that are classified as " + specifiedGenre + " are as follows:\n\n" + songsOfGenre);
        }
        else
        {
            //If songsOfGenre is empty, there are no songs that are of the specified genre
            output("There are no songs in this collection of the specified genre.");
        }
    }

    public void deleteAlbum_Menu()
    {
        /*
        This method shows a list of all albums that exist and asks the user to pick one to be deleted
         */

        //Declare the variable for the album that the user wants to delete. An integer number 1, 2, 3
        int deleteAlbumSelection = 0;
        //The name of the album the user wants to delete will be pulled out for a confirmation message
        String deletedAlbumName;

        /*First call the getAlbumSelection method. This puts together a list of existing albums and asks the user to
        select one. It returns an integer number 1 being album1, 2 album2, 3 album3 or -1 return to main menu
         */
        deleteAlbumSelection = getAlbumSelection("Select which album to delete:");
        if (deleteAlbumSelection == -1)
            //If the user has selected return to main menu the the variable deleteAlbumSelection will be == -1
            return;

        /*
        The album has now been selected, use the integer number that has been entered to copy the correct album into
        the varable tempAlbum
         */
        switch (deleteAlbumSelection)
        {
            case 1:
            {
                deletedAlbumName = album1.getName();
                break;
            }
            case 2:
            {
                deletedAlbumName = album2.getName();
                break;
            }
            default:
                deletedAlbumName = album3.getName();
        }

        /*
         Make sure the user is sure they want to delete the album. They must explicitly type the album name,
         otherwise the method will give the user an error message and return to main menu as a failed attempt
         */
        if (input("Please type \"" + deletedAlbumName + "\" below to confirm deletion").compareTo(deletedAlbumName) != 0)
        {
            output("Album deletion failed");
            return;
        }

        /*
        The album will be deleted and the remaining albums shuffled down so that empty albums are always at the end
        This fact is important for adding new albums, as it is assumed in other methods that empty albums are at the
        end. As a result, the following will apply:

        if album1 is to be deleted:
            - album1 becomes album2 (Essentially deleting the contents of album1)
            - album2 becomes album3
            - album3 can now be set to null meaning it is ready to be made into a new album
        if album2 is to be deleted:
            - album1 is unaffected
            - album2 becomes album3 (Essentially deleting the contents of album2)
            - album3 can now be set to null meaning it is ready to be made into a new album
        if album3 is to be deleted:
            - album1 and album2 are unaffected
            - album3 can now be set to null meaning it is ready to be made into a new album
         */
        switch (deleteAlbumSelection)
        {
            case 1:
            {
                //Album1 is to be deleted, see algorithm above
                album1 = album2;
                album2 = album3;
                album3 = null;
                break;
            }
            case 2:
            {
                //Album2 is to be deleted, see algorithm above
                album2 = album3;
                album3 = null;
                break;
            }
            case 3:
            {
                //Album3 is to be deleted, see algorithm above
                album3 = null;
                break;
            }
        }

        //Show confirmation message
        output("Album \"" + deletedAlbumName + "\" deleted succesfully!");
    }

    public void deleteSong_Menu()
    {
        /*
        This method shows a list of songs in alphabetical order and asks the user to select one to be deleted
         */

        /*
        A String of length 2 will be returned by the method getSongToDelete(). The first character will be a 1, 2, 3
        for which album the song to be deleted is from, and the second character will be 1, 2, 3, 4 for which song
        the user wishes to delete. Store this String in the variable albumSongCode
         */
        String albumSongCode;

        /*
        Once the user has selected which song they want to delete, the album that contains that song will be copied
        to a temporary album to be edited, and this will be copied back to the original at the end
         */
        Album tempAlbum;

        /*
        The method getSongToDelete() presents the user with a menu of all existing songs in alphabetical order, and
        asks the user to select one. It will return a String of length 2, the first character referring to the album
        and the second character referring to the song. If the user chooses to return to main menu from this method,
        albumSongCode will receive the value "-1"
         */
        albumSongCode = getSongToDelete();

        //If albumSongCode == "-1" The user wishes to return to main menu
        if (albumSongCode.compareTo("-1") == 0)
        {
            return;
        }

        /*
        By taking the charAt(0) this takes the first character of the String albumSongCode. This refers to which
        album contains the song the user wishes to delete. Using a switch case we can take this number 1, 2, 3
        and copy the correct album into the variable tempAlbum where it can be edited easier.
         */
        switch (albumSongCode.charAt(0))
        {
            case '1':
            {
                //The album is album 1, so copy album1 into tempAlbum
                tempAlbum = album1;
                break;
            }
            case '2':
            {
                //The album is album 2, so copy album2 into tempAlbum
                tempAlbum = album2;
                break;
            }
            default:
            {
                //If it is not album1 or album2, the album must be album 3, so copy album3 into tempAlbum
                tempAlbum = album3;
                break;
            }
        }

        /*
        Now convert the second character in the albumSongCode to an integer. The second character in albumSongCode
        refers to the song number. This integer can be passed into the Album Class's Method getSongName() and get the
        user-entered name for that song.
         */
        int songNumber = Integer.parseInt(String.valueOf(albumSongCode.charAt(1)));

        /*
        With the songNumber and the tempAlbum we can now pull the name of the song that the user wishes to delete
        out and store it the String songName where it can be shown to the user for confirmation
         */
        String songName = tempAlbum.getSongName(songNumber);

        /*
        Ask the user to confirm that they have selected the correct song. Ask them to enter the name of the song.
        If they mistype, the song will not be deleted. They must enter it explicitly.
         */
        if (input("Please type \"" + songName + "\" below to confirm deletion").compareTo(songName) != 0)
        {
            //If they have not typed the song name correctly, notify the user and return to the main menu
            output("Song deletion failed");
            return;
        }

        /*
        With the confirmation out of the way, call the Album Class's method deleteSong and pass in the integer for
        which song to delete
         */
        tempAlbum.deleteSong(songNumber);

        //Now that the song has been deleted, copy tempAlbum back into the correct album that it came from initially
        switch (albumSongCode.charAt(0))
        {
            case '1':
            {
                //tempAlbum was album1, so copy it back now
                album1 = tempAlbum;
                break;
            }
            case '2':
            {
                //tempAlbum was album2, so copy it back now
                album2 = tempAlbum;
                break;
            }
            default:
            {
                //tempAlbum was album3, so copy it back now
                album3 = tempAlbum;
                break;
            }
        }

        //Show a success message
        output("Song deleted successfully");
    }

    //--------------------- Input/Output methods ---------------------
    public String input(String message)
    {
        /*
         * This method shows up a text box with the data in the variable "message" and returns what the user enters in
         * the form of a string. It will loop until a valid input has been entered
         */

        //Declare the variable to hold the input the user enters
        String userInput = null;
        /*
        If the user clicks cancel the result will be null. If the user clicks OK but hasn't typed anything, the result
        will be an empty string. Both of these are invalid, so loop until the user types something and then presses OK
         */
        while ((userInput == null) || (userInput.length() == 0))
        {
            userInput = JOptionPane.showInputDialog(null, message, "Song Collection", JOptionPane.INFORMATION_MESSAGE);
        }
        //Return what the user typed as a String
        return userInput;
    }

    public void output(String message)
    {
        /*
         * This method displays the data in the variable "message" for the user, along with an OK button. Programme
         * will hang until the OK button has been pressed.
         */

        JOptionPane.showMessageDialog(null, message, "Song Collection", JOptionPane.INFORMATION_MESSAGE);
    }

    //--------------------- Checking methods ---------------------
    public boolean isInteger(String temp)
    {
        /*
         * This method takes a string that has been entered by the user, and attempts to parse it into an integer
         * variable. If a NumberFormatException is thrown, the variable is not an integer and therefore the function
         * returns false. If the parse is successful it returns true
         */

        try
        {
            // Declare a new integer and attempt to parse the argument "temp" into it
            int i = Integer.parseInt(temp);
            // If the above line doesn't throw any errors, return true
            return true;
        } catch (NumberFormatException e)
        {
            // If a number format exception is thrown, return false. The argument "temp" is not an integer
            return false;
        }
    }

    public boolean validMenuEntry(String userInput, int lowOption, int highOption)
    {
        /*
         * This method takes three arguments. A user input, and then a low option and a high option. Because the menus
         * in this programme are displayed numerically and the user is expected to type in a number, the low option is
         * the smallest number available on the menu (usually 1), and the high option is the largest number on the menu,
         * or the last option that a user could pick. This method first checks that the value the user entered is an
         * integer using the isInteger method. After that it checks that it is within the acceptable range, that is
         * between the low option and high option. Returns true if it is a valid entry and false if it is invalid.
         */

        if (isInteger(userInput) && (Integer.parseInt(userInput) >= lowOption) && (Integer.parseInt(userInput) <= highOption))
        {
            return true;
        }
        else
        {
            /*
            Either the user has entered something that is not an integer, or they have entered a number that does not
            exist on the menu (less than 1 or more than the number of items in the menu)
             */
            output("Please enter a valid menu item number");
            return false;
        }

    }

    //--------------------- Data Obtaining ---------------------
    public int getAlbumSelection(String promptingMessage)
    {
        /*
        This method builds a database of which albums exist (Have been initialized so far) and displays these along
        with a return to main menu option to the user. The user is then prompted to select an album or select return to
        main menu, and then the function returns either the album number 1, 2, 3 or returns -1 if the user requests the
        main menu
         */

        /*
        Keep track of the number of albums that have been initialized. If this is still 0 by the end of the method, no
        albums exist.
         */
        int numberOfAlbums = 0;
        /*
        This method is called from a number of different places throughout the program. Therefore a prompting message
        is provided that is specific to where this method is called from. Add to the String menuOptions which will keep
        track of the menu that will be displayed to the user
         */
        String menuOptions = promptingMessage + "\n";
        // The menu item that the user selects will be stored here initially
        String menuUserInput;
        // menuUserInput will be converted to an integer and stored here
        int albumSelection;

        //First check whether an album exists, and if it does put together add its name to menuOptions.
        if (album1 != null)
        {
            //Increment numberOfAlbums to keep track of how many currently exist
            numberOfAlbums++;
            menuOptions += "1. " + album1.getName() + "\n";
        }
        //Repeat the above if statement for album2
        if (album2 != null)
        {
            numberOfAlbums++;
            menuOptions += "2. " + album2.getName() + "\n";
        }
        //Repeat the above if statement for album3
        if (album3 != null)
        {
            numberOfAlbums++;
            menuOptions += "3. " + album3.getName() + "\n";
        }

        /*
        If numberOfAlbums is still 0, there have been no albums created yet. In this case we cannot proceed, so notify
        the user and then return to main menu
         */
        if (numberOfAlbums == 0)
        {
            output("You haven't created any albums! Please create some albums first.");
            /*
            A return value of -1 will tell the method that this was called from that it should simply return the user
            to the main menu
             */
            return -1;
        }

        //All the valid albums are now stored in menuOptions. Next add a return to main menu option to this menu
        menuOptions += String.valueOf(numberOfAlbums + 1) + ". Return to main menu\n";

        //With the menu complete, display to the user and loop until an album is selected
        do
        {
            menuUserInput = input(menuOptions + "What do you wish to do?");
        }
        /*
        Using the validMenuEntry method, check that the user enters an integer number that corresponds the the menu.
        numberOfAlbums is increased by 1 as the user could specify return to main menu
         */
        while (!validMenuEntry(menuUserInput, 1, numberOfAlbums + 1));

        //With a valid input from the user, convert it to an integer
        albumSelection = Integer.parseInt(menuUserInput);

        //Check whether the user wishes to return to main menu
        if (albumSelection == numberOfAlbums + 1)
        {
             /*
            A return value of -1 will tell the method that this was called from that it should simply return the user
            to the main menu
             */
            return -1;
        }
        else
        {
            /*
            If the user has not selected main menu, return the number of the album that was selected (1 for album1, 2
            for album2 or 3 for album3)
             */
            return albumSelection;
        }
    }

    public int convertDurationToSecs(String userInput)
    {
        /*
        This method will take a duration in the format mm:ss and return an integer of that duration in seconds.
        In the case of an invalid format, the method will return -1.
        */

        /*
        Start by declaring the variables to store the Strings for the Minute portion and the Second portion, and the
        index within the user's input that represents where the colon is
         */
        String minPortion;
        String secPortion;
        int colonIndex;

        /*
        First check that a colon is present in the input the user has provided. If there is no colon, the variable
        colonIndex will be == -1.
         */
        colonIndex = userInput.indexOf(":");

        /*
         * if colonIndex == -1 this means there is no colon and it is therefore is not a valid input
         * Also check that the user has only entered one colon. If the last index is the same as the first index,
         * There is one colon. If they are different, there is at least two colons. This method will return if there is
         * either no colon, or more than one colon. An invalid piece of data is returned as a -1 to differentiate
         * between a valid duration.
         */
        if ((colonIndex == -1) || (userInput.lastIndexOf(":") != colonIndex))
        {
            return -1;
        }

        //There is a single colon present, so now take the first half for "minutes" portion
        minPortion = userInput.substring(0, colonIndex);

        /*
        And take the second half for the "seconds" portion. A substring call with one argument will take from the
        specified index to the end of the string
         */
        secPortion = userInput.substring(colonIndex + 1);

        /*
        Check both portions seperately. If either of them is not an integer, the user has entered invalid data, in
        which case return -1
         */
        if (!isInteger(minPortion) || (!isInteger(secPortion)))
        {
            return -1;
        }

        //Finally check that the second portion is not greater than 59 seconds. If it is, return -1
        if (Integer.parseInt(secPortion) > 59)
        {
            return -1;
        }

        //Finally calculate the duration and return
        return Integer.parseInt(minPortion) * 60 + Integer.parseInt(secPortion);
    }

    public String getGenreSelection(String promptingMessage)
    {
        /*
        This method asks the user to enter a number 1, 2, 3, 4 to pick a genre. It is used in several parts of the
        program.
         */

        //Declare the variable to store the menu item selected by the user
        String menuInput;

        /*
        This method is called in different parts of the program and so prompting message may be different depending on
        which method has called it. Regardless, the menu itself will be the same as there are only 4 options for genre
         */
        promptingMessage = promptingMessage + "\n1: Rock\n2: Pop\n3: Hip-hop\n4: Bossa nova";

        //Ask the user for the genre
        do
        {
            menuInput = input(promptingMessage);
        }
        //Loop until a valid response has been entered
        while (!validMenuEntry(menuInput, 1, 4));

        //menuInput is currently a menu number so use a switch case to return the correct genre
        switch (menuInput)
        {
            case "1":
            {
                return "Rock";
            }
            case "2":
            {
                return "Pop";
            }
            case "3":
            {
                return "Hip-hop";
            }
            default:
            {
                /*
                By this point it has bene confirmed that the menuInput is valid which means it is either 1, 2, 3, or 4.
                If it is not the other 3 it is safe to assume it must be the last one with a default case
                 */
                return "Bossa nova";
            }
        }
    }

    public String getSongToDelete()
    {
        /*
        This Method gathers a list of all the songs that have been created so far and puts them into alphabetical order.
        It will then display a menu to the user and ask them to select a song, or return to the main menu. If the user
        selects a song, this method will return a string of length 2. The first character in this String refers to
        which album the song is from - 1, 2, 3. The second character refers to the song number 1, 2, 3, 4. For the rest
        of this method's documentation, this String will be referred to as the Album Song Code. If the user selects to
        return to main menu, this method returns "-1".

        As the songs are searched and ordered into alphabetical order, they will be added into this reference. They
        will added in the format of Album Song Codes. As a result this reference will be a record of where each of the
        songs come from, except ordered in alphabetical order.
         */
        String songsInMenuReference = "";
        /*
        The algorithm to order the songs into alphabetical order makes use of the ASCII value of the characters. This
        variable will keep track of the lowest ASCII value it has found so far. The lowest value at the end is the one
        that comes first alphabetically
         */
        int recordBest;
        /*
        In addition to keeping track of the best ASCII code found, the Album Song Code for that song needs to also be
        recorded so that it can be added to the songsInMenuReference at the end
         */
        String recordBestCode;
        /*
        Imagine that all the possible songs that can be added (12 all up - 3 albums and 4 songs in each) are assigned
        a character in the following string. album1 song1 is the first character, Album2 song2 is the 6th character etc.
        After a song has been added to songsInMenuReference, the corresponding character in this String will be changed
        to a 1 so that it doesn't get added to songsInMenuReference multiple times
         */
        String songAddedToReferenceRecord = "000000000000";
        /*
        Once the songsInMenuReference has been completed, the menu will start to be built to be shown to the user. The
        following variable will keep track of the numbers along the left hand side of the menu as it is being built
         */
        int buildMenuItemNumber;
        /*
        As part of building the menu, the song number will need to be pulled out of the songsInMenuReference to be
        passed to the Album Class's method getSongName() so that the song name can be presented in the menu
         */
        int buildMenuSongNumber;
        //This String will hold the actual menu options for when the user is finally asked to enter a song to delete
        String menu;
        //Store the input from the user when they make a selection
        String userInput;
        //The user input will be converted to an integer to perform some mathematical operations
        int userInputInt;

        /*
        Start by building the songsInMenuReference String. This algorithm works by looping 12 times for 12 songs, and
        for each loop, compare the songs against each other 12 times for 12 songs. In other words, every time the i
        loop runs, a new song will be added to the songsInMenuReference. Each time the j loop runs, a new song will be
        tested to see if it comes before the previous best song alphabetically. Every j loop it first checks if album1
        exists, and if so it checks all for songs in album1 (Hence the k loop looping 1, 2, 3, 4). After that it checks
        album2, and then album3.
         */

        for (int i = 0; i < 12; i++)
        {
            /*
            Declare a dummy recordBest. An ASCII > 255 cannot be typed on a keyboard so there's no way the user has
            entered this as a song name. The first song the user has entered will immediately beat this score, and run
            from there.
             */
            recordBest = 255;
            //The best Album Song Code found so far will be stored here and updated if a new best if found
            recordBestCode = "";
            //As described above, loop 12 times to compare all 12 songs
            for (int j = 0; j < 12; j++)
            {
                //Check if album1 exists before checking the songs inside
                if (album1 != null)
                {
                    //Check all 4 songs in album1 with this for loop
                    for (int k = 1; k <= 4; k++)
                    {
                        /*
                        The method songExists takes an integer 1, 2, 3, 4 and returns a true or false if that
                        particular song has been created or is null
                         */
                        if (album1.songExists(k))
                        {
                            /*
                            If the song exists, convert the song name to lower case and check its ASCII code against
                            the record best. If it is lower, the song in question is earlier in the alphabet than the
                            last "best" song
                             */
                            if ((int) album1.getSongName(k).toLowerCase().charAt(0) < recordBest)
                            {
                                /*
                                If the song is better than the last best song, also check that it has not been
                                already added to the songsInMenuReference. If it has already been added, the char in
                                songsAddedToMenuReference will hold the value of '1'. If it has not yet been added, it
                                will be '0'. String references start at 0 so subtract 1 from k to get the correct index
                                 */
                                if (songAddedToReferenceRecord.charAt(k - 1) != '1')
                                {
                                    /*
                                    The song is the best found so far, that is the song whose first character has an
                                    ASCII value lower than all the other ones that have been checked so far. Store this
                                    ASCII value, and also the Album Song Code before continuing to check other songs.
                                     */
                                    recordBest = album1.getSongName(k).toLowerCase().charAt(0);
                                    //The "1" refers to album1. The value of k will be the song number
                                    recordBestCode = "1" + String.valueOf(k);
                                }
                            }
                        }
                    }
                }

                /*
                The following block is almost identical to that of album1 so will not be documented explicitly except
                for changes
                 */
                if (album2 != null)
                {
                    for (int k = 1; k <= 4; k++)
                    {
                        if (album2.songExists(k))
                        {
                            if ((int) album2.getSongName(k).toLowerCase().charAt(0) < recordBest)
                            {
                                /*
                                The songsAddedToReferenceRecord is one String that keeps track of all 12 songs. As k in
                                this loop can be either 1, 2, 3, or 4, the char we need to check in this case needs to
                                account for the 4 songs in album1. Hence add 3 to k (album1 indexes are 0, 1, 2, 3)
                                 */
                                if (songAddedToReferenceRecord.charAt(k + 3) != '1')
                                {
                                    recordBest = album2.getSongName(k).toLowerCase().charAt(0);
                                    recordBestCode = "2" + String.valueOf(k);
                                }
                            }
                        }
                    }
                }

                /*
                The following block is almost identical to that of album1 so will not be documented explicitly except
                for changes
                 */
                if (album3 != null)
                {
                    for (int k = 1; k <= 4; k++)
                    {
                        if (album3.songExists(k))
                        {
                            if ((int) album3.getSongName(k).toLowerCase().charAt(0) < recordBest)
                            {
                                /*
                                The songsAddedToReferenceRecord is one String that keeps track of all 12 songs. As k in
                                this loop can be either 1, 2, 3, or 4, the char we need to check in this case needs to
                                account for the 8 songs in album1 and album2. Hence add 7 to k (album1 indexes are 0,
                                1, 2, 3, whilst album2 indexes are 4, 5, 6, 7)
                                 */
                                if (songAddedToReferenceRecord.charAt(k + 7) != '1')
                                {
                                    recordBest = album3.getSongName(k).toLowerCase().charAt(0);
                                    recordBestCode = "3" + String.valueOf(k);
                                }
                            }
                        }
                    }
                }
            }

            /*
            After the j loop has been completed, the recordBestCode should hold the Album Song Code of the song that
            comes earliest in the alphabet, and has not already been added to the songsInMenuReference. As a result
            add it now. Note it could still be an empty String "" which would indicate that all songs that exist have
            already been added into the reference, but concatenating an empty String will not change anything.
             */
            songsInMenuReference += recordBestCode;

            /*
            Check the length of recordBestCode. As described above, if it is an empty String that means that all songs
            have been already added to the songsInMenuReference. If it is not empty, it is an Album Song Code referring
            to the next song alphabetically.
             */
            if (recordBestCode.length() != 0)
            {
                /*
                To calculate which character to change in the songAdddedToReferenceRecord, subtract one from album part
                of the Album Song Code, and then multiply by 4. This puts us in place of the starting point for each
                album in the reference record. Either 0 for album1, 4 for album2, or 8 for album3. Then subtract 1 from
                the song part of the Album Song Code and add that, which will align us with the correct index
                 */
                int temp = (Integer.parseInt(String.valueOf(recordBestCode.charAt(0))) - 1) * 4 + Integer.parseInt(String.valueOf(recordBestCode.charAt(1))) - 1;
                /*
                Changing the character to a "1" will indicate that the song has already been added, and it won't be
                added multiple times. Take the substring before the index, add a "1", and add the substring after the
                index to create a new version of songAddedToReferenceRecord, with the new value "1" in place
                 */
                songAddedToReferenceRecord = songAddedToReferenceRecord.substring(0, temp) + "1" + songAddedToReferenceRecord.substring(temp + 1);
            }
            else
            {
                /*
                If the length of recordBestCode is 0, then all songs that exist have been added to the
                songsInMenuReference already. There's no reason to keep looping, as no more songs will be added
                 */
                break;
            }
        }

        /*
        If after all the for loops the songsInMenuReference is empty, there mustn't have been any songs to add,
        that is they haven't been created yet. Notify the user and return a "-1" which will return the user to the main
        menu
         */
        if (songsInMenuReference.length() == 0)
        {
            output("There are no songs in this collection.");
            return "-1";
        }

        /*
        Now with the songsInMenuReference complete, start constructing the menu that will be shown to the user

        Start with a prompting message
         */
        menu = "What song do you wish to delete?\n\n";
        /*
        This variable will be the numbers down the left side of the menu. Start at 0 because it is incremented
        immediately in the following for loop so it will be 1 before it is added to the menu.
         */
        buildMenuItemNumber = 0;

        /*
        Loop through the songsInMenuReference 2 at a time because this reference holds Album Song Codes which are of
        length 2. This loop will look through the reference and for each loop add one new song to the menu.
         */
        for (int i = 0; i < songsInMenuReference.length(); i += 2)
        {
            //Increment the menu item number so that there is a discrete number for each option in the menu
            buildMenuItemNumber++;

            //To the menu String, add the above menu item number
            menu += buildMenuItemNumber + ". ";

            /*
            Find the song number by looking at the songsInMenuReference index i+1 (i is the album part, i+1 is
            the song part)
             */
            buildMenuSongNumber = Integer.parseInt(String.valueOf(songsInMenuReference.charAt(i + 1)));

            //With the song number recorded, find out which album the song in question is from using a switch case
            switch (songsInMenuReference.charAt(i))
            {
                case '1':
                {
                    /*
                    If the character is a 1, the song is from album1. Add the song name using the Album Class Method
                    getSongName() which takes an index 1, 2, 3, 4 and returns the user-entered song name. Also add the
                    album name in brackets
                     */
                    menu += album1.getSongName(buildMenuSongNumber) + " (" + album1.getName() + ")\n";
                    break;
                }
                case '2':
                {
                    /*
                    If the character is a 2, the song is from album2. Add the song name using the Album Class Method
                    getSongName() which takes an index 1, 2, 3, 4 and returns the user-entered song name. Also add the
                    album name in brackets
                     */
                    menu += album2.getSongName(buildMenuSongNumber) + " (" + album2.getName() + ")\n";
                    break;
                }
                case '3':
                {
                    /*
                    If the character is a 3, the song is from album3. Add the song name using the Album Class Method
                    getSongName() which takes an index 1, 2, 3, 4 and returns the user-entered song name. Also add the
                    album name in brackets
                     */
                    menu += album3.getSongName(buildMenuSongNumber) + " (" + album3.getName() + ")\n";
                    break;
                }
            }
        }

        //Increment buildMenuItemNumber once more so that an option for return to main menu can be added to the menu
        buildMenuItemNumber++;
        //Add the menu number and the appropriate phrase to the menu
        menu += buildMenuItemNumber + ". Return to main menu";

        //With the menu completed, show it to the user and request an input
        do
        {
            userInput = input(menu);
        }
        //If the user does not enter an integer number that is a valid option on the menu, loop until they do
        while (!validMenuEntry(userInput, 1, buildMenuItemNumber));

        //If the user has chosen the menu option "return to main menu", return a "-1" which will indicate this.
        if (Integer.parseInt(userInput) == buildMenuItemNumber)
        {
            return "-1";
        }

        //Convert the user entered menu option to an integer
        userInputInt = Integer.parseInt(userInput);

        /*
        Because the menu started at 1 but String indexes start at 0, subtract 1 from what the user entered. Next,
        multiply by 2 as the songsInMenuReference contains 2 character long Album Song Codes.
         */
        userInputInt = (userInputInt - 1) * 2;

        //Finally, return the Album Song Code for the song that the user wishes to delete.
        return songsInMenuReference.substring(userInputInt, userInputInt + 2);
    }
}