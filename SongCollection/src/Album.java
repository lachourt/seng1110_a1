/**
 * This class is used by the SongCollection program to store Song objects that will be added and manipulated through
 * the SongCollection class. Each album can contain 4 songs up to a duration of 720 seconds (12 minutes).
 *
 * @Author: Lachlan Court
 * @Modified 04/05/2020
 * @Version 1.0
 * @StudentNumber: c3308061
 */

public class Album
{
    // instance variables 
    private String name;
    private Song song1, song2, song3, song4;
    private int totalTime;
    //The total duration of songs on the album cannot exceed 12 minutes. 12*60=720
    private final int MAX_TIME = 720;

    /*
      Constructor method takes a string for the name of the album and sets the totalTime counter to 0
    */
    public Album(String _name)
    {
        name = _name;
        totalTime = 0;
    }

    //--------------------- Getters ---------------------
    public String getName()
    {
        return name;
    }

    public int getTotalTime()
    {
        return totalTime;
    }

    public int getMaxTime()
    {
        return MAX_TIME;
    }

    //--------------------- Checking methods ---------------------
    public boolean isFull()
    {
        /*
        This method checks the current object to see if all the song variables have been initialized or not. Songs that
        don't exist yet will be the value "null". Return true if all 4 songs exist and false if there is 1 or more free
        songs.
         */
        return ((song1 != null) && (song2 != null) && (song3 != null) && (song4 != null));
    }

    public boolean hasDuplicate(Song newSong)
    {
        /*
        This method takes a song object as an argument and checks it against the other songs in the album object to see
        if it already exists.
         */

        /*
        For each song, check that it first exists, then if the song on the album matches the new one that was passed
        in as an argument. If any of them match, return true, otherwise if it is a unique song return false
         */
        return (((song1 != null) && (isSameSong(song1, newSong))) || ((song2 != null) && (isSameSong(song2, newSong))) || ((song3 != null) && (isSameSong(song3, newSong))) || ((song4 != null) && (isSameSong(song4, newSong))));
    }

    public boolean songExists(int songNumber)
    {
        /*
        This method takes an integer argument referring to a song in the album (1 is song1, 2 is song2 etc). It returns
        true if that particular song exists, and false if it does not exist (holds the value of "null")
         */

        /*
        Check first if the argument passed refers to song1. Then check if song1 exists by comparing it to "null". If it
        is null, return false, if it is not null, return true.
         */
        if (songNumber == 1)
        {
            if (song1 != null)
            {
                return true;
            }
            return false;
        }
        // If the argument passed was not the integer 1, check 2, 3, 4
        if (songNumber == 2)
        {
            if (song2 != null)
            {
                return true;
            }
            return false;
        }
        if (songNumber == 3)
        {
            if (song3 != null)
            {
                return true;
            }
            return false;
        }
        if (songNumber == 4)
        {
            if (song4 != null)
            {
                return true;
            }
            return false;
        }
        return true;
    }

    //--------------------- Menu Methods ---------------------
    public void addSong(Song newSong)
    {
        /*
        This method takes a new song object and adds it into this album object.

        At this point it has already been checked that there is a free spot on the album, so there is no need to check
        again. The album fills from the left, that is, try to add to song1 first, if that is full try song2, if that is
        full try song3, then song4.
         */
        if (song1 == null) song1 = newSong;
        else if (song2 == null) song2 = newSong;
        else if (song3 == null) song3 = newSong;
        else song4 = newSong;
        /*
        The totalTime has also already been checked, so no need to check again. Update the total time to ensure that
        songs that are too big to fit on the album cannot be added at the same time as this new one
         */
        totalTime += newSong.getIntDuration();
    }

    public String showSongs(boolean showDetails)
    {
        /*
        This method is called from a number of areas in the program and as such takes a boolean argument referring to
        what kind of data should be returned. The method can return either a string of just song names, in which case
        the showDetails argument would be false, or a string of song names with all the song details, in which case
        showDetails would be true. It also formats the strings so that they will be presented neatly and indented
         */

        //This will keep track of all the details as they are added, and will be returned in one go at the end
        String songDetails = "";
        /*
        This variable will keep track of how many songs exist. If it is still 0 at the end of this method, the message
        should change to let the user know there are no songs on the album
         */
        int numberOfSongs = 0;

        /*
        First add an opening statement, but only if the method will be returning all the song details. If it is
        returning only song names, they will be under the name of the album, and as such the following message will be
        implied
         */
        if (showDetails)
        {
            songDetails = "The following songs are on this album\n\n";
        }

        //Check each song individually. If it exists, add some data to songDetails
        if (song1 != null)
        {
            //First add the name of the song. If showDetails is false, this is all the text that will be added.
            songDetails += song1.getName();
            //Only add the details if showDetails is true
            if (showDetails)
            {
                /*
                Add the details of the songs, as well as some blank space at the start and end of each line of details
                to indent a little bit for neat formatting
                 */
                songDetails += "\n    Artist: " + song1.getArtist() + "\n    ";
                songDetails += "Genre: " + song1.getGenre() + "\n    ";
                songDetails += "Duration: " + song1.getStringDuration() + "\n\n";
            }
            //If show details is false, add a newline and indent ready for the next song
            else
            {
                songDetails += "\n    ";
            }
            /*
            Increment numberOfSongs to show that songs have been added to the songDetails variable. If this is still 0
            at the end of the method then it is clear that no songs have been added.
             */
            numberOfSongs++;
        }
        //Repeat the above if statement for songs 2, 3, 4
        if (song2 != null)
        {
            songDetails += song2.getName();
            if (showDetails)
            {
                songDetails += "\n    Artist: " + song2.getArtist() + "\n    ";
                songDetails += "Genre: " + song2.getGenre() + "\n    ";
                songDetails += "Duration: " + song2.getStringDuration() + "\n\n";
            }
            else
            {
                songDetails += "\n    ";
            }
            numberOfSongs++;
        }
        if (song3 != null)
        {
            songDetails += song3.getName();
            if (showDetails)
            {
                songDetails += "\n    Artist: " + song3.getArtist() + "\n    ";
                songDetails += "Genre: " + song3.getGenre() + "\n    ";
                songDetails += "Duration: " + song3.getStringDuration() + "\n\n";
            }
            else
            {
                songDetails += "\n    ";
            }
            numberOfSongs++;
        }
        if (song4 != null)
        {
            songDetails += song4.getName();
            if (showDetails)
            {
                songDetails += "\n    Artist: " + song4.getArtist() + "\n    ";
                songDetails += "Genre: " + song4.getGenre() + "\n    ";
                songDetails += "Duration: " + song4.getStringDuration() + "\n\n";
            }
            else
            {
                songDetails += "\n    ";
            }
            numberOfSongs++;
        }
        /*
        If the method has reached this point and numberOfSongs is still 0, no songs have been added. In this case
        replace the songDetails string with a message to notify the user of this
         */
        if (numberOfSongs == 0)
        {
            songDetails = "There are no songs on this album.";
        }
        //Finally return the songDetails string
        return songDetails;
    }

    public String getSongsUnderDuration(int specifiedDuration)
    {
        /*
        This method receives a song duration as an integer of seconds and returns a formatted String of all the songs
        that have a duration less than the one specified
         */

        //This will be a running String of all the songs under the specified duration, to be returned at the end
        String songsUnderDuration = "";

        /*
        For each song, check first that it exists, and then test the duration. If it is under the specified duration,
        add the song name to songsUnderDuration. Add newline characters for formatting purposes
         */
        if ((song1 != null) && (song1.getIntDuration() < specifiedDuration))
        {
            songsUnderDuration += song1.getName() + "\n";
        }
        if ((song2 != null) && (song2.getIntDuration() < specifiedDuration))
        {
            songsUnderDuration += song2.getName() + "\n";
        }
        if ((song3 != null) && (song3.getIntDuration() < specifiedDuration))
        {
            songsUnderDuration += song3.getName() + "\n";
        }
        if ((song4 != null) && (song4.getIntDuration() < specifiedDuration))
        {
            songsUnderDuration += song4.getName() + "\n";
        }

        //Finally return the result to the original method
        return songsUnderDuration;
    }

    public String getSongsOfGenre(String specifiedGenre)
    {
        /*
        This method receives a genre of songs and returns all the songs that are classified as that genre as a
        formatted String
         */

        //This will be a running String of all the songs of the specified genre, to be returned at the end
        String songsOfGenre = "";

        /*
        For each song, check first that it exists, and then check the genre. If it is the same as the specified genre,
        add the song name to songsOfGenre. Add newline characters for formatting purposes
         */
        if ((song1 != null) && (song1.getGenre().compareTo(specifiedGenre) == 0))
        {
            songsOfGenre += song1.getName() + "\n";
        }
        if ((song2 != null) && (song2.getGenre().compareTo(specifiedGenre) == 0))
        {
            songsOfGenre += song2.getName() + "\n";
        }
        if ((song3 != null) && (song3.getGenre().compareTo(specifiedGenre) == 0))
        {
            songsOfGenre += song3.getName() + "\n";
        }
        if ((song4 != null) && (song4.getGenre().compareTo(specifiedGenre) == 0))
        {
            songsOfGenre += song4.getName() + "\n";
        }

        //Finally return the result to the original method
        return songsOfGenre;
    }

    public String getSongName(int songNumber)
    {
        /*
        This method takes a song number and returns the name of that song. This method is not called unless it is known
        that the song exists, so there is no reason to check.
         */

        switch (songNumber)
        {
            case 1:
            {
                return song1.getName();
            }
            case 2:
            {
                return song2.getName();
            }
            case 3:
            {
                return song3.getName();
            }
            /*
            This method will not run if the song in question does not exist due to previous checks. As such is it is
            not the first three songs, it must be the fourth one
             */
            default:
            {
                return song4.getName();
            }
        }
    }

    public void deleteSong(int songNumber)
    {
        /*
        This method takes a song number as an integer and deletes that song.

        Use a switch case to determine which song to delete from the argument songNumber
         */
        switch (songNumber)
        {
            case 1:
            {
                /*
                First subtract the song1 duration from the totalTime so that the totalTime variable is an accurate
                representation of the duration of the songs currently on the album
                 */
                totalTime -= song1.getIntDuration();
                /*
                Songs are filled in the album from the left, that is song1, then song2, then song3, then song4. In
                order to ensure this concept is maintained, when a song is deleted, all the other songs need to be
                shuffled to the left to ensure that the empty songs in the album are always at the end.
                 */
                song1 = song2;
                song2 = song3;
                song3 = song4;
                song4 = null;
                break;
            }
            //Repeat for a songNumber of 2, 3, and 4
            case 2:
            {
                totalTime -= song2.getIntDuration();
                song2 = song3;
                song3 = song4;
                song4 = null;
                break;
            }
            case 3:
            {
                totalTime -= song3.getIntDuration();
                song3 = song4;
                song4 = null;
                break;
            }
            case 4:
            {
                totalTime -= song4.getIntDuration();
                song4 = null;
                break;
            }
        }
    }

    //--------------------- Private Methods ------------------------------
    private boolean isSameSong(Song a, Song b)
    {
        /*
        This method takes two songs and compares them to see if they are the same or not. It is only used by other
        methods within the Album class therefore the method is private.

        Critera for a duplicate song are name, artist and duration need to be the same. Check each of these and return
        true if they all match or false if any of them are different
         */
        return ((a.getName().toLowerCase().compareTo(b.getName().toLowerCase()) == 0) && (a.getArtist().toLowerCase().compareTo(b.getArtist().toLowerCase()) == 0) && (a.getIntDuration() == b.getIntDuration()));
    }
}